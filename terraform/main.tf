provider "aws" {
  region = "us-west-2"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "1.46.0"

  name = "ecs-vpc"
  cidr = "10.50.0.0/16"

  azs             = ["us-west-2a", "us-west-2b"]
  public_subnets  = ["10.50.11.0/24", "10.50.12.0/24"]
  private_subnets = ["10.50.21.0/24", "10.50.22.0/24"]

  single_nat_gateway = true

  enable_nat_gateway   = true
  enable_vpn_gateway   = false
  enable_dns_hostnames = true

  tags = {
    Terraform = "true"
  }
}

resource "aws_security_group" "lb_sg" {
  name        = "load-balancer-sg"
  description = "Allow all inbound traffic to http and https"
  vpc_id      = "${module.vpc.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "load-balancer-sg"
  }
}

resource "aws_security_group" "ecs_service_sg" {
  name        = "ecs-service-sg"
  description = "Allow all inbound traffic to service port"
  vpc_id      = "${module.vpc.vpc_id}"

  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = ["${aws_security_group.lb_sg.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "fargate-ecs-service-sg"
  }
}

module "ecs" {
  source  = "blinkist/airship-ecs-cluster/aws"
  version = "0.5.0"

  name       = "production"
  vpc_id     = "${module.vpc.vpc_id}"
  subnet_ids = ["${module.vpc.private_subnets}"]

  # create_roles defines if we create IAM Roles for EC2 instances
  create_roles = true

  # create_autoscalinggroup defines if we create an ASG for ECS
  create_autoscalinggroup = true

  vpc_security_group_ids = ["${aws_security_group.ecs_service_sg.id}", "${aws_security_group.lb_sg.id}"]

  cluster_properties {
    create                 = false
    ec2_key_name           = "daniel"
    ec2_instance_type      = "t2.micro"
    ec2_asg_min            = 1
    ec2_asg_max            = 1
    ec2_disk_size          = 50
    ec2_disk_type          = "gp2"
    ec2_disk_encryption    = "false"
    ec2_custom_userdata    = ""
    block_metadata_service = false
    efs_enabled            = "0"
    efs_id                 = ""
  }
}

data "aws_acm_certificate" "cert" {
  domain      = "*.danielschaaff.com"
  types       = ["AMAZON_ISSUED"]
  most_recent = true
}

module "alb_shared_services_external" {
  source                    = "terraform-aws-modules/alb/aws"
  version                   = "3.4.0"
  load_balancer_name        = "ecs-external"
  security_groups           = ["${aws_security_group.lb_sg.id}"]
  load_balancer_is_internal = false
  logging_enabled           = false
  subnets                   = ["${module.vpc.public_subnets}"]
  vpc_id                    = "${module.vpc.vpc_id}"
  https_listeners           = "${list(map("certificate_arn", "${data.aws_acm_certificate.cert.arn}", "port", 443))}"
  https_listeners_count     = "1"
  http_tcp_listeners        = "${list(map("port", "80", "protocol", "HTTP"))}"
  http_tcp_listeners_count  = "1"
  target_groups             = "${list(map("name", "default-ext", "backend_protocol", "HTTP", "backend_port", "80"))}"
  target_groups_count       = "1"
}

data "aws_route53_zone" "zone" {
  name = "danielschaaff.com"
}

module "ecs_service" {
  source  = "blinkist/airship-ecs-service/aws"
  version = "0.8.6"

  name = "php-hello"

  ecs_cluster_id = "${module.ecs.cluster_id}"

  region = "us-west-2"

  fargate_enabled = false

  # awsvpc_enabled            = true
  # awsvpc_subnets            = ["${module.vpc.private_subnets}"]
  # awsvpc_security_group_ids = ["${aws_security_group.ecs_service_sg.id}"]

  load_balancing_type = "application"
  load_balancing_properties {
    # The ARN of the ALB, when left-out the service, 
    lb_arn = "${module.alb_shared_services_external.load_balancer_id}"

    # https listener ARN
    lb_listener_arn_https = "${element(module.alb_shared_services_external.https_listener_arns,0)}"

    # http listener ARN
    lb_listener_arn = "${element(module.alb_shared_services_external.http_tcp_listener_arns,0)}"

    # The VPC_ID the target_group is being created in
    lb_vpc_id = "${module.vpc.vpc_id}"

    create_route53_record = true
    route53_zone_id       = "${data.aws_route53_zone.zone.zone_id}"

    # health_uri defines which health-check uri the target 
    # group needs to check on for health_check, defaults to /ping
    health_uri = "/foo"

    health_matcher = "400-404"

    # Creates a listener rule which redirects to https
    redirect_http_to_https = true
  }
  container_cpu             = 2
  container_memory          = 128
  container_port            = 80
  bootstrap_container_image = "nginx:stable"
  # force_bootstrap_container_image to true will 
  # force the deployment to use var.bootstrap_container_image as container_image
  # if container_image is already deployed, no actual service update will happen
  force_bootstrap_container_image = false
  # Initial ENV Variables for the ECS Task definition
  # container_envvars {
  #   ENV_VARIABLE = "SOMETHING"
  # }
  # capacity_properties defines the size in task for the ECS Service.
  # Without scaling enabled, desired_capacity is the only necessary property
  # defaults to 2
  # With scaling enabled, desired_min_capacity and desired_max_capacity 
  # define the lower and upper boundary in task size
  capacity_properties {
    desired_capacity = "1"
  }
  # By default we enable KMS and SSM, for this demo we don't need it.
  ssm_enabled = false
  kms_enabled = false
}
