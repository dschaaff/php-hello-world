<?php

use HelloWorld\Hello;

use PHPUnit\Framework\TestCase;
class HelloTest extends TestCase
{
  public function testworld(): void
  {
      $expected = "Hello World";
      $actual = Hello::world();
      $this->assertEquals($expected, $actual);
  }
}