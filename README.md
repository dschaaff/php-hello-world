# PHP Hello World Docker Pipeline

## Why does this exist?

This is a demonstration of a simple build pipeline for a docker based php app running on AWS ECS.
This app does nothing but print Hello World. Composer isn't used for the app, but it used as example in the pipeline.

## Terraform

The terraform directory contains the terraform code used to spin up the AWS infrastructure. Public modules
were used to save time.

## Gitlab CI

This is a sample pipeline 

- runs tests with php unit 
- builds and pushes a docker container tagged with $branch-$short_commit_sha
- triggers rolling deploy on staging cluster
- manual step to deploy through to production